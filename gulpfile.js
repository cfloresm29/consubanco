'use strict';
/*
|-------------------------------------------------------------------------------------
|    Include Gulp & Tools We'll Use
|-------------------------------------------------------------------------------------
*/
var gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    del = require('del'),
    runSequence = require('run-sequence'),
    browserSync = require('browser-sync'),
    pagespeed = require('psi'),
    reload = browserSync.reload,
    site = 'http://project';

/*
|-------------------------------------------------------------------------------------
|    Lint javascript
|-------------------------------------------------------------------------------------
*/
gulp.task('jshint', function() {
    // http://jslinterrors.com/did-you-mean-to-return-a-conditional
    // Expected an assignment or function call and instead saw an expression.
    var options = {
        "-W093": true,
        "expr": true
    };

    return gulp.src('app/js/main.js')
        .pipe(reload({stream: true, once: true}))
        .pipe($.jshint( options ))
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.if(!browserSync.active, $.jshint.reporter('fail')));
});
/*
|-------------------------------------------------------------------------------------
|    Optimize images
|-------------------------------------------------------------------------------------
*/
gulp.task('images', function() {
    return gulp.src('app/img/**/*')
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('dist/img'))
        .pipe($.size({title: 'images'}));
});
/*
|-------------------------------------------------------------------------------------
|    Copy files to dist app
|-------------------------------------------------------------------------------------
|------------------------------------------------
|    Copy all files at the root level (app)
|------------------------------------------------
*/
gulp.task('copy:root', function() {
    return gulp.src([
            'app/*',
            'node_modules/apache-server-configs/dist/.htaccess',
            '!app/index.html',
            '!app/assets'
        ], {
            dot: true
        }).pipe(gulp.dest('dist'))
        .pipe($.size({title: 'copy:root'}));
});
/*
|------------------------------------------------
|    Copy extra scripts
|------------------------------------------------
*/
gulp.task('copy:scripts', function() {
    return gulp.src([
            'app/assets/modernizr/modernizr.js',
            'app/assets/jquery/dist/jquery.min.js'
        ])
        .pipe($.uglify({preserveComments: 'some'}))
        .pipe(gulp.dest('dist/js'))
        .pipe($.size({title: 'copy:script'}));
});
/*
|------------------------------------------------
|    Output Copy files
|------------------------------------------------
*/
gulp.task('copy', ['copy:root', 'copy:scripts']);
/*
|-------------------------------------------------------------------------------------
|    Copy Web Fonts To Dist
|-------------------------------------------------------------------------------------
*/
gulp.task('fonts', function () {
    return gulp.src(['app/fonts/**'])
        .pipe(gulp.dest('dist/fonts'))
        .pipe($.size({title: 'fonts'}));
});
/*
|-------------------------------------------------------------------------------------
|    Compile styles
|-------------------------------------------------------------------------------------
*/
gulp.task('styles', function() {
    return gulp.src('app/styles/*.scss')
        .pipe($.changed('styles',{ extension: '.scss' }))
        .pipe($.sourcemaps.init())
        .pipe($.sass({ precision: 10 }))
        .pipe($.sourcemaps.write())
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('.tmp/styles'))
        // Concatenate And Minify Styles
        .pipe($.if('*.css', $.csso()))
        .pipe(gulp.dest('dist/styles'))
        .pipe($.size({ title: 'styles'}));
});
/*
|-------------------------------------------------------------------------------------
|    Scan Your HTML For Assets & Optimize Them
|-------------------------------------------------------------------------------------
*/
gulp.task('html', function() {
    var assets = $.useref.assets({searchPath: 'app'});

    return gulp.src('app/index.html')
        .pipe(assets)
        // Concatenate And Minify JavaScript
        .pipe($.if('*.js', $.uglify({preserveComments: 'some'})))
        .pipe(assets.restore())
        .pipe($.useref())
        // Update Production Path
        .pipe($.replace('assets/modernizr', 'js'))
        .pipe($.replace('assets/jquery/dist', 'js'))
        // Minify Any HTML
        .pipe($.if('*.html', $.minifyHtml({empty: true, conditionals: true})))
        // Output Files
        .pipe(gulp.dest('dist'))
        .pipe($.size({title: 'html'}));
});
/*
|-------------------------------------------------------------------------------------
|    Clean Output Directory
|-------------------------------------------------------------------------------------
*/
gulp.task('clean', del.bind(null, ['.tmp','dist']));
/*
|-------------------------------------------------------------------------------------
|    Server     |   Watch Files for Changes & Reload
|-------------------------------------------------------------------------------------
|------------------------------------------------
|    Development
|------------------------------------------------
*/
gulp.task('serve', ['styles'], function() {
    browserSync({
        notify: false,
        logPrefix: 'Project Development',
        browser: 'google chrome canary',
        server: ['.tmp','app']
    });

    gulp.watch(['app/index.html'], reload);
    gulp.watch(['app/styles/**/*.scss'], [ 'styles', reload]);
    gulp.watch(['app/js/**/*.js'], ['jshint']);
    gulp.watch(['app/img/**/*'], reload);
});
/*
|------------------------------------------------
|    Production    |  Output from the dist build
|------------------------------------------------
*/
gulp.task('serve:dist', ['default'], function(cb) {
    browserSync({
        notify: false,
        logPrefix: 'Project Production',
        browser: 'google chrome canary',
        port: 4000,
        server: 'dist'
    });

    runSequence('zip', cb);
});
/*
|-------------------------------------------------------------------------------------
|   zip     |   Compress Production Ready Files
|-------------------------------------------------------------------------------------
*/
gulp.task('zip', function() {
    return gulp.src(['dist/**/*','!dist/.DS_Store'], {dot: true})
        .pipe($.zip('site.zip'))
        .pipe(gulp.dest(''))
        .pipe($.size({title: 'zip'}));
});
/*
|-------------------------------------------------------------------------------------
|    Pagespeed Insight
|-------------------------------------------------------------------------------------
*/
gulp.task('pagespeed', pagespeed.bind(null, {
    // key: '', // https://developers.google.com/speed/docs/insights/v1/getting_started
    nokey: true,
    url: site,
    locale: 'es_MX',
    strategy: 'desktop' // desktop or mobile
}));
/*
|-------------------------------------------------------------------------------------
|    Default task    |   Build Production Files
|-------------------------------------------------------------------------------------
*/
gulp.task('default', ['clean'], function(cb) {
    runSequence('styles', ['jshint', 'html', 'copy'], cb);
});








