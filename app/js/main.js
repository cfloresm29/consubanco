/* !
|
|-------------------------------------------------------------------------------------
|	Project Web Application
|-------------------------------------------------------------------------------------
*/
;(function(window,document,undefined) {
	'use strict';
	/*
	|-------------------------------------------------------------------------------------
	|	variables
	|-------------------------------------------------------------------------------------
	*/
	var _resizeTimer, currentPosition;
	/*
	|-------------------------------------------------------------------------------------
	|	Globals
	|-------------------------------------------------------------------------------------
	*/
	window._currentPosition = currentPosition = window.location.hash || '#home';
	/*
	|-------------------------------------------------------------------------------------
	|	Window events
	|-------------------------------------------------------------------------------------
    |--------------------------------------------
    |	OnLoad
    |--------------------------------------------
    */
    window.onload = function() {
    	console.log('Page Loaded');
    };
    /*
    |--------------------------------------------
    |	OnResize
    |--------------------------------------------
    */
	window.onresize = function() {
        // RESIZE DELAY
        if ( _resizeTimer ) clearTimeout( _resizeTimer ); // clear any previous pending timer
        _resizeTimer = setTimeout(function() {
            _resizeTimer = null; // set new timer
            // ------------------------------------------------------
            // Code goes here

            // ------------------------------------------------------
        }, 250);
	};
	/*
	|-------------------------------------------------------------------------------------
	|	Functions
	|-------------------------------------------------------------------------------------
	*/
	function _setWindowLocation(node) {
        var fx = null,
        	hash = null;

        if ( node.length ) {
            hash = node.attr('id');

            node.attr( 'id', '' );

            window.location.hash = hash;

            node.attr( 'id', hash );
        }
    }
	/*
    |-------------------------------------------------------------------------------------
    |	Load Async Fonts (IIFE)
    |-------------------------------------------------------------------------------------
    */
    // ;(function loadFonts () {
    function loadFonts () {
        	window.WebFontConfig = {
        		google: {
        	        families: [ 'Kaushan Script:400' ]
        	    }
        	};
    		var wf = document.createElement("script");

        	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        	    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        	wf.async = 'true';

        	document.getElementsByTagName('head')[0].appendChild(wf);
    }
    // }());
	/*
	|-------------------------------------------------------------------------------------
	|	Plugin Manipulation
	|-------------------------------------------------------------------------------------
	*/
	// new Imager({
	// 	// lazyload: true,
	// 	availableWidths: {
	// 		300: 'sm',
	// 		600: 'md',
	// 		800: 'lg'
	// 	},
	// 	availablePixelRatios: [1, 2]
	// });
	/*
	|-------------------------------------------------------------------------------------
	|-------------------------------------------------------------------------------------
	*/
}(this,this.document));
/*
|-------------------------------------------------------------------------------------
|	Load Google Analytics
|-------------------------------------------------------------------------------------
*/
// ;(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
//             function(){(b[l].q=b[l].q||[]).push(arguments);});b[l].l=+new Date();
//             e=o.createElement(i);r=o.getElementsByTagName(i)[0];
//             e.src='//www.google-analytics.com/analytics.js';
//             r.parentNode.insertBefore(e,r);}(window,document,'script','ga'));
//             ga('create','UA-XXXXXXXX-1','auto');ga('send','pageview');








